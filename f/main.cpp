﻿#include <GL/freeglut.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

#include "CObiekt.h"
#include <list>



class CPole: public CObiekt
{
	 private:
        double szerokosc;
        double wysokosc;

    public:
        CPole(void);
        CPole(double szerokosc, double wysokosc);
        ~CPole(void);
        void Rysuj();
};
CPole::CPole()
{
    this->szerokosc = 0.2;
    this->wysokosc = 0.4;
}

CPole::CPole(double szerokosc, double wysokosc)
{
    this->szerokosc = szerokosc;
    this->wysokosc = wysokosc;
}

CPole::~CPole()
{
}




void CPole::Rysuj()
{
    glColor3d(this->kolor.red, this->kolor.green, this->kolor.blue);

    glPushMatrix();
        glTranslated(this->translacja[0], this->translacja[1], this->translacja[2]);
        glRotated(this->rotacja[0], 1.0, 0.0, 0.0);
        glRotated(this->rotacja[1], 0.0, 1.0, 0.0);
        glRotated(this->rotacja[2], 0.0, 0.0, 1.0);
        glBegin(GL_POLYGON);
        {
            glVertex3d(this->szerokosc/2, -this->wysokosc/2, 0.0);
            glVertex3d(this->szerokosc/2, this->wysokosc/2, 0.0);
            glVertex3d(-this->szerokosc/2, this->wysokosc/2, 0.0);
            glVertex3d(-this->szerokosc/2, -this->wysokosc/2, 0.0);
		
        }
			
        glEnd();
    glPopMatrix();
}





//////////////////////////////////////////////

/*****************************************************
*                Zmienne globalne                     *
******************************************************/
const int glutWindowWidth = 640;
const int glutWindowHeight = 480;
float proportion = (float) glutWindowWidth / (float) glutWindowHeight;

int i=0;
CPole*pole[25];
CPole*klocek;
double zmiennaA;
double zmiennaB;
//vector <CPotwor*> potwory;

/* GLUT callback Handlers */
static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;
    proportion = ar;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 5.0, 10.0);
    gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

static void idle(void)
{
    glutPostRedisplay();
}

static void display(void)
{
    // przyk�ad na uzyskanie czasu
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;

    // wyczyszenie sceny
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_LIGHTING);

    glPushMatrix();
	
	klocek->Rysuj();
	for(int a=0;a<=i;a++)
	{
		if(pole[a]!=NULL)
	{
		pole[a]->Rysuj();
	
	}
	}


    glPopMatrix();

    glutSwapBuffers();
}

void Keyboard(unsigned char key, int x, int y)
{

	 if (key == 'p')
	 {
		 
	 }
	 if (key == 'o')
	 {
		 zmiennaA=klocek->translacja[0];
		zmiennaB=klocek->translacja[1];
		 pole[i]->translacja[0]=zmiennaA;
		 pole[i]->translacja[1]=zmiennaB;
		 //pole[0]->Przesun(0.4, -0.5, 0.0);
		i++;
	
	 }

	 if(key=='q')
		 {
			exit(0);
			
		}
	 if(key=='a')
		 {
			 if(klocek->translacja[1]>-0.8)
			 klocek->Przesun(-0.1,0.0,0.0);
	
			
		}
	 if(key=='d')
		 {
			 if(klocek->translacja[1]>-0.8)
			klocek->Przesun(0.1,0.0,0.0);
		}

	 if(key=='r')
		 {
			
		}
	 if(key=='t')
		 {
			
			
		}
	 
}



void logika () {
       
        // if((obiekt->translacja[0]<xglobalnie+0.1)&(obiekt->translacja[0]>xglobalnie-0.1)&(obiekt->translacja[1]<yglobalnie+0.1)&(obiekt->translacja[1]>yglobalnie-0.1))
        //{
        //     
        //    bohater->translacja[1]+=0.1;
        //    obiekt->LosowePolozenie();
        //}
         
}
void mouse(int button, int state, int x, int y)
{
    
   

   
}

void timer(int czas) {

	

    //cout<<"Timer:"<< czas<<endl;
    glutTimerFunc(100,timer,czas+1);

	if(klocek->translacja[1]>-0.8)
	klocek->Przesun(0.0,-0.1,0.0);
	
	if(klocek->translacja[1]<-0.8)
	{
		 //klocek->Przesun(0.0, 1.6, 0.0);
		 klocek->translacja[1]=0.8;
		 klocek->translacja[0]=0.0;

		
	 }
	
	if(klocek->translacja[1]<-0.7)
		{
		 zmiennaA=klocek->translacja[0];
		zmiennaB=klocek->translacja[1];
		 pole[i]->translacja[0]=zmiennaA;
		 pole[i]->translacja[1]=zmiennaB;
		 //pole[0]->Przesun(0.4, -0.5, 0.0);
		i++;
	
	 }

	for(int a=0;a<=i;a++)
	{
		if(klocek->translacja[1]<(pole[a]->translacja[1]+0.1)&&klocek->translacja[0]==pole[a]->translacja[0])
		{	 

			 
			klocek->translacja[1]=0.8;
			klocek->translacja[0]=0.0;
		}
	}


	for(int a=0;a<=i;a++)
	{
		if(klocek->translacja[1]==(pole[a]->translacja[1]+0.1)&&klocek->translacja[0]==pole[a]->translacja[0])
		{	 
				zmiennaA=klocek->translacja[0];
				zmiennaB=klocek->translacja[1];
				pole[i]->translacja[0]=zmiennaA;
				pole[i]->translacja[1]=zmiennaB;
				//pole[0]->Przesun(0.4, -0.5, 0.0);
				i++;
			
		}
	}




}


int main(int argc, char *argv[])
{
	  srand (time(NULL));
  
    
	
	
		
		
	klocek= new CPole(0.1,0.1);
	klocek->Przesun(0.0, 0.8, 0.0);
	for(int j=0;j<25;j++)
	{
	pole[j]= new CPole(0.1,0.1);
	pole[j]->Przesun(0.0, -2, 0.0);
	}
	
	
	

    glutInitWindowSize(glutWindowWidth, glutWindowHeight);
    glutInitWindowPosition(40,40);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

    glutCreateWindow("OpenGLUT Shapes");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(Keyboard);
    glutTimerFunc(100,timer,0);
  // glutPassiveMotionFunc(mousepassive);
   // glutMouseFunc(mouse);
   

    glutSetOption ( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION ) ;

    glClearColor(1,1,1,1);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);

    glutMainLoop();
    system("pause");
	delete pole;
    return EXIT_SUCCESS;
}