#include "CObiekt.h"
#include <stdlib.h>
#include <iostream>
using namespace std;
void CObiekt::Ukryj()
{
        ukryj = true;
}

void CObiekt::Pokaz()
{
        ukryj = false;
}

void CObiekt::Przesun(double dX, double dY, double dZ)
{
        translacja[0] += dX;
        translacja[1] += dY;
        translacja[2] += dZ;
}

void CObiekt::UstawPozycje(double x, double y, double z)
{
        translacja[0] = x;
        translacja[1] = y;
        translacja[2] = z;
}

void CObiekt::Obroc(double rotX, double rotY, double rotZ)
{
        rotacja[0] += rotX;
        rotacja[1] += rotY;
        rotacja[2] += rotZ;
}

void CObiekt::UstawKolor(double red, double green, double blue)
{
        kolor.red = red;
        kolor.green = green;
        kolor.blue = blue;
}

void CObiekt::LosowePolozenie()
{

    double pozycjaRandx,pozycjaRandy;
    pozycjaRandx=-1+(double)(rand() / (RAND_MAX + 1.0) * 2);
    pozycjaRandy=-1+(double)(rand() / (RAND_MAX + 1.0) * 2);

    this->UstawPozycje(pozycjaRandx,pozycjaRandy,0);

}